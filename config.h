/* appearance */
static const int sloppyfocus        = 1;  /* focus follows mouse */
static const unsigned int borderpx  = 1;  /* border pixel of windows */
static const int lockfullscreen     = 1;  /* 1 will force focus on the fullscreen window */
static const int smartborders       = 1;
static const float rootcolor[]      = {0.3, 0.3, 0.3, 1.0};
static const float bordercolor[]    = {0.5, 0.5, 0.5, 1.0};
static const float focuscolor[]     = {1.0, 0.0, 0.0, 1.0};
/* To conform the xdg-protocol, set the alpha to zero to restore the old behavior */
static const float fullscreen_bg[]  = {0.1, 0.1, 0.1, 1.0};

/* cursor warping */
static const bool cursor_warp = false;

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* app_id     title       tags mask     isfloating  isterm  noswallow monitor scratchkey */
	/* examples:
 	{ "Gimp",     NULL,       0,            1,          0,      1,        -1,     0   },
	{ "firefox",  NULL,       1 << 8,       0,          0,      1,        -1,     0   },
	*/
        { NULL,       "sphtop",   0,            1,          0,      1,        -1,    'z'  },
        { NULL,       "spterm",   0,            1,          0,      1,        -1,    'x'  },
        { NULL,       "sppmxr",   0,            1,          0,      1,        -1,    'c'  },
        { NULL,       "spblue",   0,            1,          0,      1,        -1,    'v'  },
        { NULL,       "spncmp",   0,            1,          0,      1,        -1,    'b'  },
        { NULL,       "spmutt",   0,            1,          0,      1,        -1,    'a'  },
        { NULL,       "spprof",   0,            1,          0,      1,        -1,    's'  },
        { NULL,       "spircc",   0,            1,          0,      1,        -1,    'd'  },
        { NULL,       "sptodo",   0,            1,          0,      1,        -1,    'f'  },
        { NULL,       "sptrem",   0,            1,          0,      1,        -1,    'g'  },
        { "alacritty",  NULL,     0,            0,          1,      1,        -1,     0   },
};

/* layout(s) */
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* monitors */
static const MonitorRule monrules[] = {
	/* name       mfact nmaster scale layout       rotate/reflect */
	/* example of a HiDPI laptop monitor:
	{ "eDP-1",    0.5,  1,      2,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL },
	*/
	/* defaults */
	{ NULL,       0.55, 1,      1,    &layouts[0], WL_OUTPUT_TRANSFORM_NORMAL },
};

/* keyboard */
static const struct xkb_rule_names xkb_rules = {
	/* can specify fields: rules, model, layout, variant, options */
	/* example:
	.options = "ctrl:nocaps",
	*/
	.options = NULL,
};

static const int repeat_rate = 25;
static const int repeat_delay = 600;

/* Trackpad */
static const int tap_to_click = 1;
static const int tap_and_drag = 1;
static const int drag_lock = 1;
static const int natural_scrolling = 0;
static const int disable_while_typing = 1;
static const int left_handed = 0;
static const int middle_button_emulation = 0;
/* You can choose between:
LIBINPUT_CONFIG_SCROLL_NO_SCROLL
LIBINPUT_CONFIG_SCROLL_2FG
LIBINPUT_CONFIG_SCROLL_EDGE
LIBINPUT_CONFIG_SCROLL_ON_BUTTON_DOWN
*/
static const enum libinput_config_scroll_method scroll_method = LIBINPUT_CONFIG_SCROLL_2FG;

/* You can choose between:
LIBINPUT_CONFIG_CLICK_METHOD_NONE       
LIBINPUT_CONFIG_CLICK_METHOD_BUTTON_AREAS       
LIBINPUT_CONFIG_CLICK_METHOD_CLICKFINGER 
*/
static const enum libinput_config_click_method click_method = LIBINPUT_CONFIG_CLICK_METHOD_BUTTON_AREAS;

/* You can choose between:
LIBINPUT_CONFIG_SEND_EVENTS_ENABLED
LIBINPUT_CONFIG_SEND_EVENTS_DISABLED
LIBINPUT_CONFIG_SEND_EVENTS_DISABLED_ON_EXTERNAL_MOUSE
*/
static const uint32_t send_events_mode = LIBINPUT_CONFIG_SEND_EVENTS_ENABLED;

/* You can choose between:
LIBINPUT_CONFIG_ACCEL_PROFILE_FLAT
LIBINPUT_CONFIG_ACCEL_PROFILE_ADAPTIVE
*/
static const enum libinput_config_accel_profile accel_profile = LIBINPUT_CONFIG_ACCEL_PROFILE_ADAPTIVE;
static const double accel_speed = 0.0;

/* Autostart */
static const char *const autostart[] = {
        "sh", "-c", "~/.config/autostart.sh", NULL,
        "pkexec", "swhkd", NULL,
        NULL /* terminate */
};

/* If you want to use the windows key change this to WLR_MODIFIER_LOGO */
#define MODKEY WLR_MODIFIER_ALT
#define TAGKEYS(KEY,SKEY,TAG) \
	{ MODKEY,                    KEY,            view,            {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_CTRL,  KEY,            toggleview,      {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_SHIFT, SKEY,           tag,             {.ui = 1 << TAG} }, \
	{ MODKEY|WLR_MODIFIER_CTRL|WLR_MODIFIER_SHIFT,SKEY,toggletag, {.ui = 1 << TAG} }

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static const char *termcmd[] = { "alacritty", NULL };
static const char *menucmd[] = { "barmenu_run", NULL };

/* named scratchpads - First arg only serves to match against key in rules*/
static const char *scratchpadcmd0[] = { "z", "alacritty", "-t", "sphtop", "-e", "htop", NULL };
static const char *scratchpadcmd1[] = { "x", "alacritty", "-t", "spterm", "-e", "zsh", NULL };
static const char *scratchpadcmd2[] = { "c", "alacritty", "-t", "sppmxr", "-e", "pulsemixer", NULL };
static const char *scratchpadcmd3[] = { "v", "alacritty", "-t", "spblue", "-e", "bluetoothctl", NULL };
static const char *scratchpadcmd4[] = { "b", "alacritty", "-t", "spncmp", "-e", "ncmpcpp", NULL };
static const char *scratchpadcmd5[] = { "a", "alacritty", "-t", "spmutt", "-e", "neomutt", NULL };
static const char *scratchpadcmd6[] = { "s", "alacritty", "-t", "spprof", "-e", "profanity", NULL };
static const char *scratchpadcmd7[] = { "d", "alacritty", "-t", "spircc", "-e", "irssi", NULL };
static const char *scratchpadcmd8[] = { "f", "alacritty", "-t", "sptodo", "-e", "todo", NULL };
static const char *scratchpadcmd9[] = { "g", "alacritty", "-t", "sptrem", "-e", "tremc", NULL };

static const Key keys[] = {
	/* Note that Shift changes certain key codes: c -> C, 2 -> at, etc. */
	/* modifier                  key                 function        argument */
      //{ MODKEY,                    XKB_KEY_p,          spawn,          {.v = menucmd} },
	{ MODKEY,                    XKB_KEY_Return,     spawn,          {.v = termcmd} },
	{ MODKEY|WLR_MODIFIER_CTRL,  XKB_KEY_z,          togglescratch,  {.v = scratchpadcmd0 } },
	{ MODKEY|WLR_MODIFIER_CTRL,  XKB_KEY_x,          togglescratch,  {.v = scratchpadcmd1 } },
	{ MODKEY|WLR_MODIFIER_CTRL,  XKB_KEY_c,          togglescratch,  {.v = scratchpadcmd2 } },
	{ MODKEY|WLR_MODIFIER_CTRL,  XKB_KEY_v,          togglescratch,  {.v = scratchpadcmd3 } },
	{ MODKEY|WLR_MODIFIER_CTRL,  XKB_KEY_b,          togglescratch,  {.v = scratchpadcmd4 } },
	{ MODKEY|WLR_MODIFIER_CTRL,  XKB_KEY_a,          togglescratch,  {.v = scratchpadcmd5 } },
	{ MODKEY|WLR_MODIFIER_CTRL,  XKB_KEY_s,          togglescratch,  {.v = scratchpadcmd6 } },
	{ MODKEY|WLR_MODIFIER_CTRL,  XKB_KEY_d,          togglescratch,  {.v = scratchpadcmd7 } },
	{ MODKEY|WLR_MODIFIER_CTRL,  XKB_KEY_f,          togglescratch,  {.v = scratchpadcmd8 } },
	{ MODKEY|WLR_MODIFIER_CTRL,  XKB_KEY_g,          togglescratch,  {.v = scratchpadcmd9 } },
	{ MODKEY,                    XKB_KEY_j,          focusstack,     {.i = +1} },
	{ MODKEY,                    XKB_KEY_k,          focusstack,     {.i = -1} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_J,          pushdown,       {0} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_K,          pushup,         {0} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_H,          incnmaster,     {.i = +1} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_L,          incnmaster,     {.i = -1} },
	{ MODKEY,                    XKB_KEY_h,          setmfact,       {.f = -0.05} },
	{ MODKEY,                    XKB_KEY_l,          setmfact,       {.f = +0.05} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_Return,     zoom,           {0} },
	{ MODKEY,                    XKB_KEY_Tab,        view,           {0} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_Q,          killclient,     {0} },
	{ MODKEY,                    XKB_KEY_t,          setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                    XKB_KEY_s,          setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                    XKB_KEY_m,          setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                    XKB_KEY_space,      setlayout,      {0} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_space,      togglefloating, {0} },
	{ MODKEY,                    XKB_KEY_f,         togglefullscreen, {0} },
	{ MODKEY,                    XKB_KEY_0,          view,           {.ui = ~0} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_parenright, tag,            {.ui = ~0} },
	{ MODKEY,                    XKB_KEY_comma,      focusmon,       {.i = WLR_DIRECTION_LEFT} },
	{ MODKEY,                    XKB_KEY_period,     focusmon,       {.i = WLR_DIRECTION_RIGHT} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_less,       tagmon,         {.i = WLR_DIRECTION_LEFT} },
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_greater,    tagmon,         {.i = WLR_DIRECTION_RIGHT} },
	TAGKEYS(          XKB_KEY_1, XKB_KEY_exclam,                     0),
	TAGKEYS(          XKB_KEY_2, XKB_KEY_at,                         1),
	TAGKEYS(          XKB_KEY_3, XKB_KEY_numbersign,                 2),
	TAGKEYS(          XKB_KEY_4, XKB_KEY_dollar,                     3),
	TAGKEYS(          XKB_KEY_5, XKB_KEY_percent,                    4),
	TAGKEYS(          XKB_KEY_6, XKB_KEY_asciicircum,                5),
	TAGKEYS(          XKB_KEY_7, XKB_KEY_ampersand,                  6),
	TAGKEYS(          XKB_KEY_8, XKB_KEY_asterisk,                   7),
	TAGKEYS(          XKB_KEY_9, XKB_KEY_parenleft,                  8),
	{ MODKEY|WLR_MODIFIER_SHIFT, XKB_KEY_E,          quit,           {0} },

	/* Ctrl-Alt-Backspace and Ctrl-Alt-Fx used to be handled by X server */
	{ WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT,XKB_KEY_Terminate_Server, quit, {0} },
#define CHVT(n) { WLR_MODIFIER_CTRL|WLR_MODIFIER_ALT,XKB_KEY_XF86Switch_VT_##n, chvt, {.ui = (n)} }
	CHVT(1), CHVT(2), CHVT(3), CHVT(4), CHVT(5), CHVT(6),
	CHVT(7), CHVT(8), CHVT(9), CHVT(10), CHVT(11), CHVT(12),
};

static const Button buttons[] = {
	{ MODKEY, BTN_LEFT,   moveresize,     {.ui = CurMove} },
	{ MODKEY, BTN_MIDDLE, togglefloating, {0} },
	{ MODKEY, BTN_RIGHT,  moveresize,     {.ui = CurResize} },
};
